<?php
class surat extends CI_model{
    public function __construct() {
        $this ->load -> database();
    }
    public function get_masuk() {
        return $this ->db ->get ('masuk') -> result_array();
    }
    public function deleteMasuk($id) {
        $this ->db ->where ('no_surat', $id);
        $this ->db ->delete('masuk');
    }
    public function masuk($no_surat) {
        return $this->db->get_where('masuk', array('no_surat' => $no_surat)) ->row();
    }
    public function update_surat($id)
    {
        $data = array(
            'no_surat' => $this->input->post('no_surat'),
            'tgl_diterima' => $this->input->post('tgl_diterima'),
            'sifat' => $this->input->post('sifat'),
            'isi_ringkas' => $this->input->post('isi_ringkas'),
            'dari' => $this->input->post('dari'),
            'kepada' => $this->input->post('kepada'),
            'diteruskan' => $this->input->post('diteruskan'),
            'pengolah' => $this->input->post('pengolah'),
            'file_surat' => $name
        );
        $this->db->where('no_surat',$id);
        return $this->db->update('masuk', $data);
    }
    public function insertmasuk() {
        $config['allowed_types'] = 'pdf|csv';
        $config['upload_path'] = './uploads/';

        $this->load->library('upload', $config);

        $this->upload->initialize($config);
        if($this->upload->do_upload('file_surat')){
            $result = $this->upload->data();
            $name = $result['file_name'];
            $data = array(
                'no_surat' => $this->input->post('no_surat'),
                'tgl_diterima' => $this->input->post('tgl_diterima'),
                'sifat' => $this->input->post('sifat'),
                'isi_ringkas' => $this->input->post('isi_ringkas'),
                'dari' => $this->input->post('dari'),
                'kepada' => $this->input->post('kepada'),
                'diteruskan' => $this->input->post('diteruskan'),
                'pengolah' => $this->input->post('pengolah'),
                'file_surat' => $name
            );
            // print_r($data); die;
            return $this->db->insert('masuk', $data);
        }
        
        
    }
    public function get_keluar() {
        return $this ->db ->get ('keluar') -> result_array();
    }
    public function deleteKeluar($id) {
        $this ->db ->where ('no_surat', $id);
        $this ->db ->delete('keluar');
    }
    public function keluar($no_surat) {
        return $this->db->get_where('keluar', array('no_surat' => $no_surat)) ->row();
    }
    public function update_surat_keluar($id)
    {
        $data = array(
            'no_surat' => $this->input->post('no_surat'),
            'tgl_diterima' => $this->input->post('tgl_diterima'),
            'sifat' => $this->input->post('sifat'),
            'prihal' => $this->input->post('isi_ringkas'),
            'kepada' => $this->input->post('kepada'),
            'dari' => $this->input->post('dari'),
        );
        $this->db->where('no_surat',$id);
        return $this->db->update('keluar', $data);
    }
    public function insertkeluar() {
        $config['allowed_types'] = 'pdf|csv';
        $config['upload_path'] = './uploads/';

        $this->load->library('upload', $config);

        $this->upload->initialize($config);
        if($this->upload->do_upload('file_surat')){
            $result = $this->upload->data();
            $name = $result['file_name'];
        $data = array(
            'no_surat' => $this->input->post('no_surat'),
            'tgl_diterima' => $this->input->post('tgl_diterima'),
            'sifat' => $this->input->post('sifat'),
            'prihal' => $this->input->post('prihal'),
            'kepada' => $this->input->post('kepada'),
            'dari' => $this->input->post('dari'),
            'file_surat' => $name
        );
        return $this->db->insert('keluar', $data);
        }
    }
    public function get_jadwal() {
        return $this ->db ->get ('jadwal') -> result_array();
    }
    public function deletejadwal($id) {
        $this ->db ->where ('kegiatan', $id);
        $this ->db ->delete('jadwal');
    }
    public function jadwal($kegiatan) {
        return $this->db->get_where('jadwal', array('kegiatan' => $kegiatan)) ->row();
    }
    public function update_jadwal($id)
    {
        $data = array(
            'kegiatan' => $this->input->post('kegiatan'),
            'waktu' => $this->input->post('waktu'),
            'tempat' => $this->input->post('tempat'),
            'disposisi' => $this->input->post('disposisi'),
        );
        $this->db->where('kegiatan',$id);
        return $this->db->update('jadwal', $data);
    }
    public function insertjadwal() {
            $data = array(
            'kegiatan' => $this->input->post('kegiatan'),
            'waktu' => $this->input->post('waktu'),
            'tempat' => $this->input->post('tempat'),
            'disposisi' => $this->input->post('disposisi'),
            );
            // print_r($data); die;
            return $this->db->insert('jadwal', $data);
        }
    public function get_dashboard() {
    return $this ->db ->get ('dashboard') -> result_array();
    }
    public function gmasuk() {
        $query = $this->db->query("SELECT COUNT(no_surat) as asu FROM masuk ");
        return $query->result_array();
    }
    public function gkeluar() {
        $query = $this->db->query("SELECT COUNT(no_surat) as asu FROM keluar");
        return $query->result_array();
    }
    public function gjadwal() {
        $query = $this->db->query("SELECT COUNT(tanggal) as asu FROM jadwal");
        return $query->result_array();
    }
    

}