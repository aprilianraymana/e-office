<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class email extends CI_Controller {

    public function masuk()
    {
        $from_email = "aprilianraymana@gmail.com";
        $to_email   = "ryanaprilian02@gmail.com";
        $this->load->library('email');
        $this->email->from($from_email, 'e-office');
        $this->email->to($to_email);
        $this->email->subject('Surat Masuk Baru');
        $this->email->message('Cek Aplikasi ya..');
        $send = $this->email->send();

        redirect('index.php/welcome/masuk');
        // $this->email->print_debugger();

    //  Send mail 
    //  if($this->email->send()){
    //         $this->session->set_flashdata("notif","Email berhasil terkirim."); 
    //  }else {
    //         $this->session->set_flashdata("notif","Email gagal dikirim."); 
    //  } 
    }

    public function keluar()
    {
        $from_email = "aprilianraymana@gmail.com";
        $to_email   = "ryanaprilian02@gmail.com";
        $this->load->library('email');
        $this->email->from($from_email, 'e-office');
        $this->email->to($to_email);
        $this->email->subject('Surat Keluar Baru');
        $this->email->message('Cek Aplikasi ya..');
        $send = $this->email->send();

        // $this->email->print_debugger();
        redirect('index.php/welcome/keluar');

    //  Send mail 
    //  if($this->email->send()){
    //         $this->session->set_flashdata("notif","Email berhasil terkirim."); 
    //  }else {
    //         $this->session->set_flashdata("notif","Email gagal dikirim."); 
    //  } 
    }
}
?>