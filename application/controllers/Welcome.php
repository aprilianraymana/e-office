<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct() {
		parent :: __construct();
		$this ->load ->model('surat');
		$this ->load ->helper('url_helper');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('component/header');
		$this->load->view('component/sidebar');
		$this->load->view('kosong');
		$this->load->view('component/footer');
	}
	public function masuk()
	{
		$data['masuk'] = $this->surat->get_masuk();
		$this->load->view('component/header');
		$this->load->view('component/sidebar');
		$this->load->view('masuk', $data);
		$this->load->view('component/footer');
	}
	public function tambahmasuk()
	{
		$this->load->view('component/header');
		$this->load->view('component/sidebar');
		$this->load->view('tambahmasuk');
		$this->load->view('component/footer');
	}
	public function insertmasuk()
	{
		$this->surat->insertmasuk();
		redirect('index.php/email/masuk');
	}
	public function deleteMasuk($id)
	{
		$this->surat->deleteMasuk($id);
		redirect('index.php/welcome/masuk');
	}
	public function masukEdit($no_surat)
	{
		$data['edit'] = $this->surat->masuk($no_surat);
		$this->load->view('component/header');
		$this->load->view('component/sidebar');
		$this->load->view('edit', $data);
		$this->load->view('component/footer');
	}
	public function editMasuk()
	{
		$id = $this->input->post('no_surat');
		$this->surat->update_surat($id);
		redirect('index.php/welcome/masuk');
	}
	public function keluar()
	{
		$data['keluar'] = $this->surat->get_keluar();
		$this->load->view('component/header');
		$this->load->view('component/sidebar');
		$this->load->view('keluar', $data);
		$this->load->view('component/footer');
	}
	public function tambahkeluar()
	{
		$this->load->view('component/header');
		$this->load->view('component/sidebar');
		$this->load->view('tambahkeluar');
		$this->load->view('component/footer');
	}
	public function insertkeluar()
	{
		$this->surat->insertkeluar();
		redirect('index.php/email/keluar');
	}
	public function deletekeluar($id)
	{
		$this->surat->deletekeluar($id);
		redirect('index.php/welcome/keluar');
	}
	public function keluarEdit($no_surat)
	{
		$data['edit'] = $this->surat->keluar($no_surat);
		$this->load->view('component/header');
		$this->load->view('component/sidebar');
		$this->load->view('editkeluar', $data);
		$this->load->view('component/footer');
	}
	public function editkeluar()
	{
		$id = $this->input->post('no_surat');
		$this->surat->update_surat_keluar($id);
		redirect('index.php/welcome/keluar');
	}
	public function jadwal()
	{
		$data['jadwal'] = $this->surat->get_jadwal();
		$this->load->view('component/header');
		$this->load->view('component/sidebar');
		$this->load->view('jadwal', $data);
		$this->load->view('component/footer');
	}
	public function tambahjadwal()
	{
		$this->load->view('component/header');
		$this->load->view('component/sidebar');
		$this->load->view('tambahjadwal');
		$this->load->view('component/footer');
	}
	public function insertjadwal()
	{
		$this->surat->insertjadwal();
		redirect('index.php/welcome/jadwal');
	}
	public function deleteJadwal($id)
	{
		$this->surat->deletejadwal($id);
		redirect('index.php/welcome/jadwal');
	}
	public function jadwalEdit($tanggal)
	{
		$data['edit'] = $this->surat->jadwal($tanggal);
		$this->load->view('component/header');
		$this->load->view('component/sidebar');
		$this->load->view('editjadwal', $data);
		$this->load->view('component/footer');
	}
	public function editJadwal()
	{
		$id = $this->input->post('tanggal');
		$this->surat->update_jadwal($id);
		redirect('index.php/welcome/jadwal');
	}
	public function dashboard()
	{
		$data['masuk'] = $this->surat->gmasuk();
		$data['keluar'] = $this->surat->gkeluar();
		$data['jadwal'] = $this->surat->gjadwal();
		// print_r($d); die;
		$this->load->view('component/header');
		$this->load->view('component/sidebar');
		$this->load->view('dashboard', $data);
		$this->load->view('component/footer');
	}
}
