<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url_helper");
        $this->load->model('mlogin');
        $this->load->library('session');
    }
    public function index_login()
    {
        $this->load->view("vlogin");
    }
    public function proses_login()
    {
        $data = array(
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password')
        );
        // print_r ($data); die;
        $hasil = $this->mlogin->cek_login($data);
        if ($hasil->num_rows() == 1) {
            foreach ($hasil->result() as $sess) {
                $sess_data['id_user'] = $sess->id_user;
                $sess_data['username'] = $sess->username;
                $sess_data['password'] = $sess->password;
                $sess_data['status_login'] = 'login';
                $sess_data['pw'] = $this->input->post('password');
                $this->session->set_userdata($sess_data);
            }
            if ($sess_data['id_user']) {
                redirect('index.php/welcome/dashboard');
            }
        }
        else
    echo"<script>alert('username atau password anda salah!'); window.location = '../login'</script>";
    }
    public function index_logout()
    {
        session_destroy();
        redirect('index.php/login/index_login');
    }
}