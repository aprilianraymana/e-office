<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Jadwal / Schedule</h3>
              <h4 class="title"><a href='<?= base_url();?>index.php/welcome/tambahjadwal/' class="btn btn-success btn-fill" type="button" id="btn-input"><i class="fa fa-plus"></i></a></h4>
              <div class="box-tools">
                <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                </div>
              </div>
            </div>
            <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="50"><center><b>No</b></center></th>
                  <th width="50"><center><b>Tanggal</b></center></th>
                  <th width="10"><center><b>Kegiatan</b></center></th>
                  <th width="20"><center><b>Waktu</b></center></th>
                  <th width="20"><center><b>Tempat</b></center></th>
                  <th width="20"><center><b>Dihadiri Oleh</b></center></th>
                  <th width="20"><center><b>Aksi</b></center></th>
                </tr>
                </thead>
                
                <tbody>
                <?php
                $no = 0;
                foreach ($jadwal as $jadwal) {
                    $no++; ?>
                <tr>
                  <td width="50"><center><b><?= $no; ?></b></center></td>
                  <td width="50"><center><b><?= $jadwal['tanggal']; ?></b></center></td>
                  <td width="50"><center><b><?= $jadwal['kegiatan']; ?></b></center></td>
                  <td width="50"><center><b><?= $jadwal['waktu']; ?></b></center></td>
                  <td width="50"><center><b><?= $jadwal['tempat']; ?></b></center></td>
                  <td width="50"><center><b><?= $jadwal['disposisi']; ?></b></center></td>
                  <td width="20"><center>
                      <a href='<?= base_url();?>index.php/Welcome/deletejadwal/<?= $jadwal['kegiatan'];?>' class='btn btn-danger'><i class='fa fa-trash'></i></a>
                      <a href='<?= base_url();?>index.php/Welcome/jadwalEdit/<?= $jadwal['kegiatan'];?>' class='btn btn-danger'><i class='fa fa-pencil'></i></a>
                  </center></td>
                </tr>
                <?php } ?>
              </tbody>
              </table>
              <script>
  $(document).ready(function() {
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
                </div>
                </div>
              
            </div>
            <!-- /.box-body -->
          </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
