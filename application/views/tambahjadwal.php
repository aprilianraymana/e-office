<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Jadwal</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('index.php/welcome/insertjadwal') ?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="datepicker">Tanggal</label>
                  <input type="date" name="tanggal" class="form-control pull-right" id="datepicker" placeholder="MM/DD/YYYY">
                </div>
                <div class="form-group">
                  <label for="inputkegiatan">Kegiatan</label>
                  <input type="text" name="kegiatan" class="form-control" id="inputkegiatan" placeholder="Kegiatan">
                </div>
                <div class="form-group">
                  <label for="inputwaktu">Waktu</label>
                  <input type="text" name="waktu" class="form-control pull-right" id="inputwaktu" placeholder="Waktu">
                </div>
                <div class="form-group">
                  <label for="inputtempat">Tempat</label>
                  <input type="text na" name="tempat" class="form-control" id="inputtempat" placeholder="Tempat">
                </div>
                <div class="form-group">
                  <label for="inputdisposisi">Disposisi</label>
                  <input type="text na" name="disposisi" class="form-control" id="inputdisposisi" placeholder="Disposisi">
                </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </form>
          </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->