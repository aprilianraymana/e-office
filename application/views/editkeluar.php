<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Surat Keluar</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="<?php echo base_url('index.php/welcome/insertkeluar') ?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputnomorsurat">Nomor Surat</label>
                  <input type="text" name="no_surat" class="form-control" id="inputnomorsurat" placeholder="Nomor Surat">
                </div>
                <div class="form-group">
                  <label for="datepicker">Tanggal Diterima</label>
                  <input type="date" name="tgl_diterima" class="form-control pull-right" id="datepicker" placeholder="MM/DD/YYYY">
                </div>
                <div class="form-group">
                  <label for="inputsifat">Sifat</label>
                  <input type="text na" name="sifat" class="form-control" id="inputsifat" placeholder="Sifat">
                </div>
                <div class="form-group">
                  <label for="inputprihal">Prihal</label>
                  <input type="text na" name="prihal" class="form-control" id="inputprihal" placeholder="Prihal">
                </div>
                <div class="form-group">
                  <label for="inputsuratdari">Surat Dari</label>
                  <input type="text na" name="dari" class="form-control" id="inputsuratdari" placeholder="Surat Dari">
                </div>
                <div class="form-group">
                  <label for="inputkepada">Kepada</label>
                  <input type="text na" name="kepada" class="form-control" id="inputkepada" placeholder="Kepada">
                </div>
                <!-- <div class="form-group">
                  <label for="exampleInputFile">Input File Surat</label>
                  <input type="file" name="file_surat" id="exampleInputFile"> -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </form>
          </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->