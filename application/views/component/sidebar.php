<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= base_url();?>assets/dist/img/avatar5.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
        <p><?= $this->session->userdata('username'); ?></p>
          <a href="<?= base_url();?>assets/#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
        </div>
      </form>
      <ul class="sidebar-menu" data-widget="tree">
      <li>
        <!-- <li class="active treeview"> -->
          <a href="<?= base_url();?>index.php/welcome/dashboard">
            <i class="fa fa-tachometer"></i> <span>Dashboard</span>
          </a>
        </li>
      <li>
        <!-- <li class="active treeview"> -->
          <a href="<?= base_url();?>index.php/welcome/dashboard">
            <i class="fa fa-tachometer"></i> <span>Data Pegawai</span>
          </a>
        </li>
        <li>
        <!-- <li class="active treeview"> -->
          <a href="<?= base_url();?>index.php/welcome/masuk">
            <i class="fa fa-envelope"></i> <span>Surat Masuk</span>
          </a>
        </li>
        <li>
        <!-- <li class="treeview"> -->
          <a href="<?= base_url();?>index.php/welcome/keluar">
            <i class="fa fa-paper-plane"></i>
            <span>Surat Keluar</span>
          </a>
        </li>
        <li>
        <!-- <li class="treeview"> -->
          <a href="<?= base_url();?>index.php/welcome/jadwal">
            <i class="fa fa-th"></i>
            <span>Jadwal / Schedule</span>
          </a>
        </li>
        <li>
        <!-- <li class="treeview"> -->
          <a href="<?= base_url();?>index.php/welcome/jadwal">
            <i class="fa fa-th"></i>
            <span>Data Pensiun</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>