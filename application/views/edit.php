<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Surat Masuk</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="<?php echo base_url('index.php/welcome/editMasuk') ?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputnomorsurat">Nomor Surat</label>
                  <input type="text" name="no_surat" class="form-control" value="<?php echo $edit->no_surat ?>" id="inputnomorsurat" placeholder="Nomor Surat">
                </div>
                <div class="form-group">
                  <label for="datepicker">Tanggal Diterima</label>
                  <input type="date" name="tgl_diterima" class="form-control pull-right" value="<?php echo $edit->tgl_diterima ?>" id="datepicker" placeholder="MM/DD/YYYY">
                </div>
                <div class="form-group">
                  <label for="inputsifat">Sifat</label>
                  <input type="text na" name="sifat" class="form-control" value="<?php echo $edit->sifat ?>" id="inputsifat" placeholder="Sifat">
                </div>
                <div class="form-group">
                  <label for="inputisiringkas">Isi Ringkas</label>
                  <input type="text na" name="isi_ringkas" class="form-control" value="<?php echo $edit->isi_ringkas ?>" id="inputisiringkas" placeholder="Isi Ringkas">
                </div>
                <div class="form-group">
                  <label for="inputsuratdari">Surat Dari</label>
                  <input type="text na" name="dari" class="form-control" value="<?php echo $edit->dari ?>" id="inputsuratdari" placeholder="Surat Dari">
                </div>
                <div class="form-group">
                  <label for="inputkepada">Kepada</label>
                  <input type="text na" name="kepada" class="form-control" value="<?php echo $edit->kepada ?>" id="inputkepada" placeholder="Kepada">
                </div>
                <div class="form-group">
                  <label for="inputditeruskan">Diteruskan</label>
                  <input type="text na" name="diteruskan" class="form-control" value="<?php echo $edit->diteruskan ?>" id="inputditeruskan" placeholder="Diteruskan">
                </div>
                <div class="form-group">
                  <label for="inputpengolah">Pengolah</label>
                  <input type="text na" name="pengolah" class="form-control" value="<?php echo $edit->pengolah ?>" id="inputpengolah" placeholder="Pengolah">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Input Surat</label>
                  <input type="file" name="file_surat" id="exampleInputFile">
                </div>
                <!-- <div class="form-group">
                  <label for="exampleInputFile">Input Surat</label>
                  <input type="file" name="file_surat" id="exampleInputFile">
                </div> -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </form>
          </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->