<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
    </section>
  <!-- /.content-wrapper -->
  <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <p>Surat Masuk</p>
          <h3><?php foreach ($masuk as $masuk) {?> <?php echo $masuk['asu'];?> <?php } ?>Surat</h3>

            </div>
            <div class="icon">
              <i class="fa fa-envelope"></i>
            </div>
            <a href="<?= base_url();?>index.php/welcome/masuk" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <p>Surat Keluar</p>
              <h3><?php foreach ($keluar as $keluar) {?> <?php echo $keluar['asu'];?> <?php } ?>Surat</h3>

            </div>
            <div class="icon">
              <i class="fa fa-paper-plane"></i>
            </div>
            <a href="<?= base_url();?>index.php/welcome/keluar" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <p>Jadwal / Schedule</p>
              <h3><?php foreach ($jadwal as $jadwal) {?> <?php echo $jadwal['asu'];?> <?php } ?>Jadwal</h3>

            </div>
            <div class="icon">
              <i class="fa fa-th"></i>
            </div>
            <a href="<?= base_url();?>index.php/welcome/jadwal" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      </div>