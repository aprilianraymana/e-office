<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Surat Keluar</h3>
              <h4 class="title"><a href='<?= base_url();?>index.php/welcome/tambahkeluar/' class="btn btn-success btn-fill" type="button" id="btn-input"><i class="fa fa-plus"></i></a></h4>
              <div class="box-tools">
                <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                </div>
              </div>
            </div>
      <div class="box-body table-responsive no-padding">
            <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="50"><center><b>No</b></center></th>
                  <th width="10"><center><b>No Surat</b></center></th>
                  <th width="20"><center><b>Tanggal Keluar</b></center></th>
                  <th width="20"><center><b>Sifat</b></center></th>
                  <th width="20"><center><b>Prihal</b></center></th>
                  <th width="10"><center><b>Kepada</b></center></th>
                  <th width="10"><center><b>Surat Dari</b></center></th>
                  <th width="20"><center><b>File Surat</b></center></th>
                  <th width="20"><center><b>Aksi</b></center></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $no = 0;
                foreach ($keluar as $keluar) {
                    $no++; ?>
                <tr>
                  <td width="50"><center><b><?= $no; ?></b></center></td>
                  <td width="50"><center><b><?= $keluar['no_surat']; ?></b></center></td>
                  <td width="50"><center><b><?= $keluar['tgl_diterima']; ?></b></center></td>
                  <td width="50"><center><b><?= $keluar['sifat']; ?></b></center></td>
                  <td width="50"><center><b><?= $keluar['prihal']; ?></b></center></td>
                  <td width="50"><center><b><?= $keluar['kepada']; ?></b></center></td>
                  <td width="50"><center><b><?= $keluar['dari']; ?></b></center></td>
                  <td width="20"><center>
                  <a href="<?= base_url();?>uploads/<?=$keluar['file_surat']; ?>" class="btn btn-warning"><i class = "fa fa-envelope-open"></i></a>
                    </center></td>
                  <td width="20"><center>
                      <a href='<?= base_url();?>index.php/Welcome/deleteKeluar/<?= $keluar['no_surat'];?>' class='btn btn-danger'><i class='fa fa-trash'></i></a>
                      <a href='<?= base_url();?>index.php/Welcome/keluarEdit/<?= $keluar['no_surat'];?>' class='btn btn-danger'><i class='fa fa-pencil'></i></a>
                  </center></td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
              <script>
  $(document).ready(function() {
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
            </div>
</div>
</div>
            <!-- /.box-body -->
          </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->