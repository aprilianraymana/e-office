-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Inang: localhost
-- Waktu pembuatan: 15 Jul 2020 pada 17.32
-- Versi Server: 5.5.25a
-- Versi PHP: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `ekantor`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE IF NOT EXISTS `jadwal` (
  `tanggal` date NOT NULL,
  `kegiatan` varchar(250) NOT NULL,
  `waktu` varchar(50) NOT NULL,
  `tempat` varchar(150) NOT NULL,
  `disposisi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`tanggal`, `kegiatan`, `waktu`, `tempat`, `disposisi`) VALUES
('2020-01-17', 'Rapat Pimpinan', '10.30 - Selesai', 'Gedung Sate', 'Kadishub Dinas Perhubungan Prov. Jabar'),
('0000-00-00', 'Rapat Pimpinan', '09.00 - 10.30', 'Ruang Rapat N250', 'Kepala Bagian & Kepala Bidang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `keluar`
--

CREATE TABLE IF NOT EXISTS `keluar` (
  `no_surat` int(11) NOT NULL,
  `tgl_diterima` date NOT NULL,
  `sifat` varchar(50) NOT NULL,
  `prihal` varchar(150) NOT NULL,
  `kepada` varchar(100) NOT NULL,
  `dari` varchar(100) NOT NULL,
  `file_surat` varchar(100) NOT NULL,
  PRIMARY KEY (`no_surat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `keluar`
--

INSERT INTO `keluar` (`no_surat`, `tgl_diterima`, `sifat`, `prihal`, `kepada`, `dari`, `file_surat`) VALUES
(202, '2020-02-02', 'Mendesak', 'Rapat Pimpinan', 'Kepala Perangkat Daerah Provinsi Jawa Barat', 'Kadishub', '2013_351.pdf'),
(552, '2019-10-01', 'penting', 'permohonan pembukaan blokir rekening atas nama sirojudin dkk', 'Ka. Kantor Cabang Bank BJB Pelabuhanratu', 'T. Laut', ''),
(903, '2020-01-08', 'Penting', 'Tindak Lanjut Penyusunan Dokumen Terkait Manajemen Risiko Untuk Program/ Kegiatan APBD TA.2O20', 'Kepala Bidang/UPTD & Kepala Subag/Seksi', ' Kepala Dinas Perhubungan ProvinsiJawa Barat', 'NODIN_-_Tindak_lanjut_Penyusunan_Dokumen_Terkait_Manajemen_Risiko.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `masuk`
--

CREATE TABLE IF NOT EXISTS `masuk` (
  `no_surat` int(11) NOT NULL,
  `tgl_diterima` date NOT NULL,
  `sifat` varchar(50) NOT NULL,
  `isi_ringkas` varchar(200) NOT NULL,
  `dari` varchar(100) NOT NULL,
  `kepada` varchar(100) NOT NULL,
  `diteruskan` varchar(100) NOT NULL,
  `pengolah` varchar(50) NOT NULL,
  `file_surat` varchar(150) NOT NULL,
  PRIMARY KEY (`no_surat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `masuk`
--

INSERT INTO `masuk` (`no_surat`, `tgl_diterima`, `sifat`, `isi_ringkas`, `dari`, `kepada`, `diteruskan`, `pengolah`, `file_surat`) VALUES
(442, '2020-01-14', 'Biasa', 'Permohonan Dokumen lnovasi Daerah ', 'BADAN PERENCANAAN PEMBANGUNAN DAERAH', 'Kepala Perangkat Daerah Provinsi Jawa Barat', 'Kepala Bidang Transportasi Darat', 'SubBag Kepegawaian & Umum', 'Permintaan_Inovasi_Daerah_untuk_PPD1.pdf'),
(510, '2019-10-01', 'biasa', 'Permohonan Izin Keramaian', 'Dinas Tanaman Pangan dan Holtikultura', 'Kadishub Prov. Jabar', 'Bid. T. Darat, UPTD P3 LLAJ WIL. II', 'sekretttttttt', ''),
(666, '2020-01-27', 'mendesak', 'Permohonan Dokumen lnovasi Daerah ', 'BADAN PERENCANAAN PEMBANGUNAN DAERAH', 'Kepala Perangkat Daerah Provinsi Jawa Barat', 'Kepala Bidang Transportasi Darat', 'SubBag Kepegawaian & Umum', '84-257-1-PB.pdf'),
(824, '2020-01-15', 'Biasa', 'Permohonan Dokumen lnovasi Daerah ', 'Darat', 'Udara', 'Kepala Bidang Transportasi Laut', 'SubBag Kepegawaian & Umum', 'DIKTAT_SISTEM_INFORMASI_MANAJEMEN_2.pdf'),
(1234, '2020-01-27', 'mendesak', 'Permohonan Dokumen lnovasi Daerah ', 'BADAN PERENCANAAN PEMBANGUNAN DAERAH', 'Kepala Perangkat Daerah Provinsi Jawa Barat', 'Kepala Bidang Transportasi Darat', 'SubBag Kepegawaian & Umum', '2013_35.pdf'),
(445566, '2020-01-16', 'penting', 'Permohonan Dokumen lnovasi Daerah ', 'Kadishub', 'Kepala Perangkat Daerah Provinsi Jawa Barat', 'Kepala Bidang Transportasi Darat', 'sekre', 'Bab10.pdf'),
(6664545, '2020-01-27', 'mendesak', 'Permohonan Dokumen lnovasi Daerah ', 'BADAN PERENCANAAN PEMBANGUNAN DAERAH', 'Kepala Perangkat Daerah Provinsi Jawa Barat', 'Kepala Bidang Transportasi Darat', 'SubBag Kepegawaian & Umum', '243-475-1-SM1.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama`, `email`, `username`, `password`, `gambar`) VALUES
(1, 'Mutia Devi', 'aprilianraymana@gmail.com', 'umum', 'umum', 'dist/img/avatar5.jpg'),
(2, 'Raymana', 'ryanaprilian02@gmail.com', 'darat', 'darat', 'dist/img/avatar2.jpg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
